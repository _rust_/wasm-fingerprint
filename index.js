const wasm = import("./pkg");

wasm
  .then(async (module) => {
    const result = await module.init();
    console.log({ result });
  })
  .catch(console.error);
