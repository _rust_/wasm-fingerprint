use crate::utils::dom::window;

pub type LocalStorage = bool;

pub fn get_local_storage() -> LocalStorage {
    let window = window();
    if let Some(_storage) = window.local_storage().unwrap() {
        true
    } else {
        false
    }
}
