use crate::utils::dom::window;

pub type IndexedDB = bool;

pub fn get_indexed_db() -> IndexedDB {
    let window = window();
    // window.session_storage()
    if let Some(_db) = window.get("indexedDB") {
        true
    } else {
        false
    }
}
