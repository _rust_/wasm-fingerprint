// #[derive(Serialize)]
// pub struct Languages {
//     language: String,
// }
pub type Languages = String;

pub fn get_languages() -> Languages {
    let window = web_sys::window().expect("no global `window` exists");
    let n = window.navigator();
    let language = n.language().unwrap();
    // let languages = n.languages();
    language
}
