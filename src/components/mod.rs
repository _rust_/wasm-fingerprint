use core::hash::Hash;
use core::hash::Hasher;
use serde::Serialize;
use std::collections::hash_map::DefaultHasher;

use self::canvas::get_canvas;
use self::canvas::Canvas;
use self::fonts::get_fonts;
use self::fonts::Fonts;
use self::indexed_db::get_indexed_db;
use self::indexed_db::IndexedDB;
use self::languages::get_languages;
use self::languages::Languages;
use self::local_storage::get_local_storage;
use self::local_storage::LocalStorage;
use self::platform::get_platform;
use self::platform::Platform;
use self::session_storage::get_session_storage;
use self::session_storage::SessionStorage;
use self::timezone::get_timezone;
use self::timezone::Timezone;

pub mod audio;
pub mod canvas;
pub mod fonts;
pub mod indexed_db;
pub mod languages;
pub mod local_storage;
pub mod platform;
pub mod session_storage;
pub mod timezone;

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Components {
    // pub audio: Audio,
    pub canvas: Canvas,
    pub fonts: Fonts,
    pub indexed_db: IndexedDB,
    pub languages: Languages,
    pub local_storage: LocalStorage,
    pub platform: Platform,
    pub session_storage: SessionStorage,
    pub timezone: Timezone,
}

impl Components {
    pub fn new() -> Self {
        Self {
            // audio: get_audio(),
            canvas: get_canvas(),
            fonts: get_fonts(),
            indexed_db: get_indexed_db(),
            languages: get_languages(),
            local_storage: get_local_storage(),
            platform: get_platform(),
            session_storage: get_session_storage(),
            timezone: get_timezone(),
        }
    }

    pub fn _calculate_hash(&self) -> u64 {
        let mut s = DefaultHasher::new();
        self.hash(&mut s);
        s.finish()
    }
}

impl Hash for Components {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.fonts.hash(state);
    }
}
