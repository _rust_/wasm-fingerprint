use std::collections::HashMap;

use wasm_bindgen::JsCast;
use web_sys::HtmlElement;

use crate::utils::{
    data::{BASE_FONTS, FONTS_LIST, TEST_STRING, TEXT_SIZE},
    dom::window,
};

pub type Fonts = Vec<String>;

pub fn get_fonts() -> Fonts {
    let body = window().document().expect("").body().expect("");
    let container = create_container();
    body.append_child(&container).expect("");

    let mut default_size = HashMap::new();
    let mut fonts = Vec::new();

    for font in BASE_FONTS {
        let span = create_span(&container, font);
        let width = span.offset_width();
        let height = span.offset_height();
        default_size.insert(font, [width, height]);
    }

    for font in FONTS_LIST {
        for base_font in BASE_FONTS {
            let detect_span = create_span_with_fonts(&container, font, base_font);
            let detect_width = detect_span.offset_width();
            let detect_height = detect_span.offset_height();

            if let Some([default_width, default_height]) = default_size.get(base_font) {
                if detect_width != *default_width || detect_height != *default_height {
                    fonts.push(font.to_string());
                    break;
                }
            }
        }
    }

    body.remove_child(&container).expect("");

    fonts
}

pub struct Element;

impl Element {
    pub fn create_element(tag: &str) -> HtmlElement {
        web_sys::window()
            .expect("")
            .document()
            .expect("")
            .create_element(tag)
            .expect("")
            .dyn_into::<HtmlElement>()
            .expect("")
    }

    pub fn set_property(el: &HtmlElement, key: &str, value: &str) {
        el.style().set_property(key, value).expect("");
    }
}

fn create_container() -> HtmlElement {
    let container = Element::create_element("div");

    Element::set_property(&container, "font-size", TEXT_SIZE);
    Element::set_property(&container, "top", "0");
    Element::set_property(&container, "left", "0");
    Element::set_property(&container, "position", "absolute");
    Element::set_property(&container, "visibility", "hidden");

    container
}

fn create_span(root: &HtmlElement, font_family: &str) -> HtmlElement {
    let span = Element::create_element("span");

    root.append_child(&span).expect("");

    Element::set_property(&span, "position", "absolute");
    Element::set_property(&span, "top", "0");
    Element::set_property(&span, "left", "0");
    Element::set_property(&span, "font-family", font_family);
    span.set_text_content(Some(TEST_STRING));
    span
}

fn create_span_with_fonts(
    root: &HtmlElement,
    font_family_to_detect: &str,
    font_family_base: &str,
) -> HtmlElement {
    let font_family = format!("'{}', {}", font_family_to_detect, font_family_base);
    create_span(root, &font_family)
}
