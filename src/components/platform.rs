pub type Platform = String;

pub fn get_platform() -> Platform {
    let window = web_sys::window().expect("no global `window` exists");
    let n = window.navigator();
    n.platform().unwrap()
}
