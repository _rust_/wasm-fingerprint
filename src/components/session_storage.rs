use crate::utils::dom::window;

pub type SessionStorage = bool;

pub fn get_session_storage() -> SessionStorage {
    let window = window();
    if let Some(_storage) = window.session_storage().unwrap() {
        true
    } else {
        false
    }
}
