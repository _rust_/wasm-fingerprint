pub type Timezone = f64;
use js_sys::Date;
pub fn get_timezone() -> Timezone {
    Date::new_0().get_timezone_offset()
}
