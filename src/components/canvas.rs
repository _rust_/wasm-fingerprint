use serde::Serialize;
use std::f64;
use wasm_bindgen::{JsCast, JsValue};
use web_sys::{CanvasRenderingContext2d, HtmlCanvasElement};

#[derive(Serialize)]
pub struct Canvas {
    winding: bool,
    geometry: String,
    text: String,
}

pub fn get_canvas() -> Canvas {
    let (canvas, context) = make_canvas_context();
    Canvas {
        winding: true,
        geometry: make_geometry_image(&canvas, &context),
        text: make_text_image(&canvas, &context),
    }
}

fn make_canvas_context() -> (HtmlCanvasElement, CanvasRenderingContext2d) {
    let document = web_sys::window().unwrap().document().unwrap();

    let canvas = document
        .create_element("canvas")
        .unwrap()
        .dyn_into::<HtmlCanvasElement>()
        .unwrap();

    let context = canvas
        .get_context("2d")
        .unwrap()
        .unwrap()
        .dyn_into::<CanvasRenderingContext2d>()
        .unwrap();

    (canvas, context)
}

fn make_text_image(canvas: &HtmlCanvasElement, context: &CanvasRenderingContext2d) -> String {
    canvas.set_width(240);
    canvas.set_height(60);
    let color = JsValue::from_str("#f60");

    context.set_text_baseline("alphabetic");
    context.set_fill_style(&color);
    context.fill_rect(100.0, 1.0, 62.0, 20.0);
    let color = JsValue::from_str("#069");
    context.set_fill_style(&color);

    context.set_font("11pt \"Times New Roman\"");
    let print_text = "Cwm fjordbank gly 😃";
    context.fill_text(print_text, 2.0, 15.0).unwrap();
    let color = JsValue::from_str("rgba(102, 204, 0, 0.2)");
    context.set_fill_style(&color);
    context.set_font("18pt Arial");
    context.fill_text(print_text, 4.0, 45.0).unwrap();

    save(canvas)
}

fn make_geometry_image(canvas: &HtmlCanvasElement, context: &CanvasRenderingContext2d) -> String {
    canvas.set_width(122);
    canvas.set_height(110);
    // let js = JsValue::as_string();

    context.set_global_composite_operation("multiply").unwrap();
    let items = [
        ("#f2f", 40.0, 40.0),
        ("#2ff", 80.0, 40.0),
        ("#ff2", 60.0, 80.0),
    ];
    for (color, x, y) in items {
        let color = JsValue::from_str(color);
        context.set_fill_style(&color);
        context.begin_path();
        context.arc(x, y, 40.0, 0.0, f64::consts::PI * 2.0).unwrap();
        context.close_path();
        context.fill();
    }

    let color = JsValue::from_str("#f9c");
    context.set_fill_style(&color);
    context
        .arc(60.0, 60.0, 60.0, 0.0, f64::consts::PI * 2.0)
        .unwrap();
    context
        .arc(60.0, 60.0, 20.0, 0.0, f64::consts::PI * 2.0)
        .unwrap();
    context.fill();

    save(canvas)
}

fn save(canvas: &HtmlCanvasElement) -> String {
    canvas.to_data_url().unwrap()
}
