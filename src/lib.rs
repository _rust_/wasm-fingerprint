mod agent;
mod components;
mod utils;

use crate::components::Components;
use agent::Agent;
use wasm_bindgen::prelude::*;

#[wasm_bindgen]
extern "C" {
    #[wasm_bindgen(js_namespace = console)]
    fn log(s: &str);
    #[wasm_bindgen(js_namespace = console)]
    fn warn(s: &str);
}

macro_rules! console_log {
    ($($t:tt)*) => (log(&format_args!($($t)*).to_string()))
}
macro_rules! _console_warn {
    ($($t:tt)*) => (warn(&format_args!($($t)*).to_string()))
}

#[wasm_bindgen()]
pub async fn init() -> Result<JsValue, JsValue> {
    console_log!("[WasmFingerprint]: INIT");
    let components = Components::new();
    let agent = Agent::new(components);

    let exports = JsValue::from_serde(&agent).unwrap();

    Ok(exports)
}
