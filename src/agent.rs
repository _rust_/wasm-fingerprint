use crate::components::Components;
use crate::utils::hashing::any_as_u8_slice;
use sha256::digest_bytes;

use serde::Serialize;

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Agent {
    pub browser_id: String,
    pub components: Components,
    pub version: String,
}

impl Agent {
    pub fn new(components: Components) -> Self {
        let bytes: &[u8] = unsafe { any_as_u8_slice(&components) };
        let browser_id = digest_bytes(bytes);
        // let browser_id = components.calculate_hash().to_string();
        Self {
            browser_id,
            components,
            version: "0.0.3".to_string(),
        }
    }
}
