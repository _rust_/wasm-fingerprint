pub mod data;
pub mod dom;
pub mod element;
pub mod hashing;

// #[wasm_bindgen(getter_with_clone)]
// #[derive(Serialize)]
// #[serde(rename_all = "camelCase")]
// pub struct ExportedNamedStructNonCopy {
//     pub non_copy_value: String,
//     pub copy_value: u32,
// }

// #[wasm_bindgen]
// #[derive(Debug, Copy, Clone)]
// pub struct MyObject {
//     pub number: u32,
//     pub string: char,
// }

// #[derive(Serialize, Deserialize)]
// #[serde(rename_all = "camelCase")]
// pub struct Example {
//     pub first_field: HashMap<String, String>,
//     pub second_field: Vec<Vec<f32>>,
//     pub third_field: [f32; 4],
// }

// #[wasm_bindgen(getter_with_clone)]
// pub struct Foo {
//     pub bar: String,
//     pub baz: String,
// }
